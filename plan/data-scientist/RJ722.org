#+TITLE: Plan
#+AUTHOR: Rahul Jha
#+EMAIL: rahul722j@gmail.com
#+TAGS: read write dev task event meeting

* GOALS
** Read these books
*** Read Neural Networks & Deep Learning by Michael A. Nielsen
    :PROPERTIES:
    :ESTIMATED: 30
    :ACTUAL:
    :OWNER: RJ722
    :ID: READ.1562245628
    :TASKID: READ.1562245628
    :END:
*** Read "Pride and Prejudice" [0/3]
    :PROPERTIES:
    :ESTIMATED: 15
    :ACTUAL:
    :OWNER: RJ722
    :ID: READ.1557995782
    :TASKID: READ.1557995782
    :END:
    - [ ] Volume the first
    - [ ] Volume the second
    - [ ] Volume the third
*** Read "Grokking Deep Learning" by Andrew Trask
    :PROPERTIES:
    :ESTIMATED: 24
    :ACTUAL:
    :OWNER: RJ722
    :ID: READ.1563523155
    :TASKID: READ.1563523155
    :END:
*** Read "The Paradoxical Prime Minister"
    :PROPERTIES:
    :ESTIMATED: 45
    :ACTUAL:
    :OWNER: RJ722
    :ID: READ.1560490772
    :TASKID: READ.1560490772
    :END:
** 100 days of writing blog posts
*** Write the posts, daily! [0/15]
    :PROPERTIES:
    :ESTIMATED: 150
    :ACTUAL:
    :OWNER: RJ722
    :ID: WRITE.1558159950
    :TASKID: WRITE.1558159950
    :END:
    - [ ] Week 7
    - [ ] Week 8
    - [ ] Week 9
    - [ ] Week 10
    - [ ] Week 11
    - [ ] Week 12
    - [ ] Week 13
    - [ ] Week 14
    - [ ] Week 15
** Create a better Portfolio
*** Brainstorm a design
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:
    :OWNER: RJ722
    :ID: DEV.1558160083
    :TASKID: DEV.1558160083
    :END:
*** Formulate further sub-goals based on the design
    :PROPERTIES:
    :ESTIMATED: 0.5
    :ACTUAL:
    :OWNER: RJ722
    :ID: TASK.1558161190
    :TASKID: TASK.1558161190
    :END:

** Complete the deeplearning.ai specialisation
** Revamp Open source contributions
*** Try working with NLTK
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:
    :OWNER: RJ722
    :ID: DEV.1558508887
    :TASKID: DEV.1558508887
    :END:
    - [ ] https://github.com/nltk/nltk/issues/1722

** Finish these courses:
*** CS224: Natural Language Processing with Deep Learning
    :PROPERTIES:
    :ESTIMATED: 35
    :ACTUAL:
    :OWNER: RJ722
    :ID: TASK.1562246395
    :TASKID: TASK.1562246395
    :END:
*** CS231N: Convolutional Neural Networks for Visual Recognition
    :PROPERTIES:
    :ESTIMATED: 30
    :ACTUAL:
    :OWNER: RJ722
    :ID: TASK.1562244243
    :TASKID: TASK.1562244243
    :END:
*** fast.ai (part-2)
    :PROPERTIES:
    :ESTIMATED: 21
    :ACTUAL:
    :OWNER: RJ722
    :ID: TASK.1562243970
    :TASKID: TASK.1562243970
    :END:
*** DeepLearning.ai: course 4 and 5
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:
    :OWNER: RJ722
    :ID: TASK.1558163905
    :TASKID: TASK.1558163905
    :END:


* PLAN
** July      16, 2019 - August     1, 2019 (17 days)
** August     2, 2019 - August    14, 2019 (13 days)
** August    15, 2019 - August    30, 2019 (16 days)
** August    31, 2019 - September 12, 2019 (13 days)
** September 13, 2019 - September 29, 2019 (17 days)
** Spetember 30, 2019 - October   12, 2019 (13 days)
** October   13, 2019 - October   28, 2019 (16 days)
** October   29, 2019 - November  11, 2019 (14 days)
** November  12, 2019 - November  27, 2019 (16 days)
** November  28, 2019 - December  11, 2019 (14 days)
** December  12, 2019 - December  26, 2019 (15 days)
** December  27, 2019 - January    9, 2020 (14 days)
** January   10, 2020 - January   25, 2020 (16 days)
** January   26, 2020 - February   8, 2020 (14 days)
** February   9, 2020 - February  24, 2020 (16 days)
** February  25, 2020 - March      8, 2020 (13 days)

